function xnew=SOR_fun(A, b)
%Prosz� o sprawdzenie wzor�w dotycz�cych metody SOR w Pa�skich wyk�adach
%Kod do testowania: 
%SOR_fun([3, 1, -1;   -1, 5, -1;   2, 4, 8;], [6; 10; 2;])

%A=[3, 1, -1;
%   -1, 5, -1;
%   2, 4, 8;];
%b=[6; 10; 2;];
%odp=[-1; 1; 2];

n=length(A);    %Rozmiar macierzy A
%Roz�o�enie macierzy A na D, L, U
for i=1:n
   for j=1:n
     if i == j
        D(i,j)=A(i,j);
     else
        D(i,j)=0;
     end
   end
end
for i=1:n
   for j=1:n
     if i<= j
        L(i,j)=0;
        U(i,j)=A(i,j);
     else
        L(i,j)=A(i,j);
        U(i,j)=0;
     end
   end
end
for i=1:n
   for j=1:n
     if i>= j
        U(i,j)=0;
     else
        U(i,j)=A(i,j);
     end
   end
end

w=2/(1+sqrt(1-(max(abs(eig(inv(D)*(L+U)))))^2));    %Wsp�czynnik relaksacji
tol=0.001;  %tolerancja
lit=0;      %liczba wykonanych iteracji
maxit=1000; %maksymalna liczba iteracji
error=999;  %b��d

odp=(A\b)'; %prawid�owe warto�� wyznaczone przy u�yciu lewostronnego operatora dzielenia

xnew=rand(n,1); %pocz�tkowe warto�ci
xold=xnew;      %pocz�tkowe warto�ci
while error>tol && lit<maxit   %p�tla ko�czy si� gdy b��d b�dzie odpowiednio ma�y lub gdy zostanie osi�gni�ta maksymalna liczba iteracji
   
    xnew= inv(D+w.*L) * ((1-w).*D-w.*U)*xold + w.*inv(D+w.*L)*b;    %obliczenie kolejnego przybli�enia
    xold=xnew;
    xnew'

    error=abs(norm(xnew)-norm(odp));    %obliczenie b��du
    lit=lit+1;
end
lit
error
odp
